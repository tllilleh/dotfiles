# Determine OS
UNAME := $(shell uname -s)

ifeq ($(UNAME), Darwin)
    OS := mac
else ifeq ($(UNAME), Linux)
    OS := linux
endif

# Determine Hostname
HOSTNAME := $(shell uname -n)

# Helper Functions
define stow
	stow -R $(1)
	if [ -d "$(1)-$(OS)" ]; then stow -R $(1)-$(OS); fi
	if [ -d "$(1)-$(HOSTNAME)" ]; then stow -R $(1)-$(HOSTNAME); fi
	if [ -d "$(1)-local" ]; then stow -R $(1)-local; fi
endef

# Top Level Targets
all: install
install: install_$(OS)

# Mac
install_mac: brew install_stow
	sudo easy_install pip
	#sudo pip install --upgrade tmuxp

# Linux
install_linux: apt install_stow neovim nerd-fonts dwm slstatus crontab rust snap
	$(call stow,dunst)
	pip3 install --user --upgrade ydiff
	pip3 install --user --upgrade pynvim
	pip3 install --user --upgrade qmk

.PHONY: rust
rust:
	if ! which rustup; \
	then \
		curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh; \
	fi

.PHONY: crontab
crontab:
	crontab crontab/crontab

# Only checkout build and install dwm if it isn't already intalled; this will
# prevent updating/breaking a dwm build managed locally if updating install
.PHONY: dwm
dwm:
	if ! which dwm; \
	then \
		if [ ! -d ".build/dwm" ]; then git clone https://git@bitbucket.org/tllilleh/dwm.git .build/dwm; fi; \
		cd .build/dwm && \
		git config user.name "Trent Lillehaugen" && \
		git config user.email "tllilleh@gmail.com" && \
		git checkout my_dwm && \
		./build.sh && \
		sudo make install && \
		cd ../.. && \
		rm -rf .build/dwm; \
	fi

# Only checkout build and install slstatus if it isn't already intalled; this will
# prevent updating/breaking a dwm build managed locally if updating install
.PHONY: slstatus
slstatus:
	if ! which slstatus; \
	then \
		if [ ! -d ".build/slstatus" ]; then git clone https://git@bitbucket.org/tllilleh/slstatus.git .build/slstatus; fi; \
		cd .build/slstatus && \
		git config user.name "Trent Lillehaugen" && \
		git config user.email "tllilleh@gmail.com" && \
		git checkout my_slstatus && \
		make && \
		sudo make install && \
		cd ../.. && \
		rm -rf .build/slstatus; \
	fi

.PHONY: nerd-fonts
nerd-fonts:
	if ! fc-list | grep "Hack Nerd Font Mono" > /dev/null; \
	then \
		if [ ! -d ".build/nerd-fonts" ]; then git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git .build/nerd-fonts; fi; \
		cd .build/nerd-fonts && \
		./install.sh -s -c Hack &&\
		./install.sh -p -c Hack &&\
		cd ../.. && \
		rm -rf .build/nerd-fonts; \
	fi

.PHONY: neovim
neovim:
	if ! which nvim; \
	then \
		mkdir -p scripts-local/bin && \
		if [ -f "scripts-local/bin/nvim.appimage" ]; then mv scripts-local/bin/nvim.appimage scripts-local/bin/nvim.appimage.backup; fi; \
		curl -L https://github.com/neovim/neovim/releases/download/stable/nvim.appimage -o scripts-local/bin/nvim.appimage && \
		chmod u+x scripts-local/bin/nvim.appimage && \
		ln -sf nvim.appimage scripts-local/bin/nvim; \
	fi
	$(call stow,scripts)

snap:
	sudo snap install alacritty --classic

apt:
	sudo apt install -y \
		imagemagick-6.q16 xdg-utils silversearcher-ag udiskie x11-xserver-utils curl exuberant-ctags stow compton xinit python3-pip xclip xcape \
	`# for dmenu` \
		suckless-tools \
	`# desktop notifications` \
		dbus-x11 libnotify-bin \
	`# applications` \
	    meld feh sxiv tmux keepassxc zathura \
	`# terminal` \
		rxvt-unicode \
	`# for screenshots` \
		maim \
	`# for clangd language server` \
		clang-tools \
		clangd \
	`# for compiling dwm` \
		libx11-dev libxft-dev libxinerama-dev \
	`# for x key mappings` \
		sxhkd xvkbd \
	`# for .Xresources to use c preprocessor ensure it is intalled` \
		cpp \
	`# for command line trash tools` \
		trash-cli \
	`# my computers don't run 24/7, use anacron` \
		anacron \
	`# for vim telescope file searching` \
		ripgrep \
	`# for app images to run` \
		libfuse2 \
	`# for notifications` \
		dunst \
	`# for dropbox` \
		nautilus-dropbox \
	`# for simple uart tasks ` \
		picocom \
	`# for scripting ssh passwords to cameras ` \
		sshpass \
	`# for controller audio volume ` \
		pavucontrol \
	`# for helping figure out how to install something ` \
		command-not-found \
	`# for tab completion in back for tools like docker, git ` \
		bash-completion \
	`# required for meld to work correctly ` \
		adwaita-icon-theme-full

brew:
ifeq (,$(wildcard /usr/local/bin/brew))
	curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install > install_brew
	/usr/bin/ruby install_brew
	rm install_brew
endif
	brew install stow tmux ydiff ctags trash-cli node ripgrep

install_stow:
	$(call stow,tmux)
	$(call stow,git)
	$(call stow,bash)
	$(call stow,gdb)
	$(call stow,scripts)
	$(call stow,i3)
	$(call stow,nvim)
	$(call stow,x)
	$(call stow,urxvt)
	$(call stow,docker)
	$(call stow,udiskie)
	$(call stow,alacritty)
