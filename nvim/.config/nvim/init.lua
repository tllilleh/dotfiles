require('set')
require('plugins')
require('remap')

-- Show yanked text with a highlight
local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd
local yank_group = augroup('HighlightYank', {})
autocmd('TextYankPost', {
    group = yank_group,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 300,
        })
    end,
})

-- Show current class/function/method in status line
local function current_treesitter_context()
	local f = require'nvim-treesitter'.statusline({
		indicator_size = 900,
		--type_patterns = {"class", "function", "method", "interface", "type_spec", "table", "if_statement", "for_statement", "for_in_statement"}
		--type_patterns = {"class", "function", "method", "interface", "type_spec", "table"}
		--type_patterns = {"class", "function", "method"}
		--type_patterns = {"function_d"},
		type_patterns = {"function_definition", "function_declaration"},
        transform_fn = function(line)
            line = line:gsub("%([^%)]*%)", "(...)")
            return line
        end
	})
	if f == nil then f = "*" end
	return string.format("%s", f) -- convert to string, it may be a empty ts node
end

function status_line()
    return table.concat {
        "%#StatusLeft#",
        "%f",
        " (%{&filetype})",
        " %h%w%m%r",
		current_treesitter_context(),
        "%=%-14.",
		"(%l,%c%V%)",
        "%P"
    }
end

vim.o.statusline = "%!luaeval('status_line()')"
