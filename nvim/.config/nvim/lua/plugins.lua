-- Show current class/function/method in status line
local function current_treesitter_context()
	local f = require'nvim-treesitter'.statusline({
		indicator_size = 900,
		--type_patterns = {"class", "function", "method", "interface", "type_spec", "table", "if_statement", "for_statement", "for_in_statement"}
		--type_patterns = {"class", "function", "method", "interface", "type_spec", "table"}
		--type_patterns = {"class", "function", "method"}
		--type_patterns = {"function_d"},
		type_patterns = {"function_definition", "function_declaration"},
        transform_fn = function(line)
            line = line:gsub("%([^%)]*%)", "(...)")
            return line
        end
	})
	if f == nil then f = "*" end
	return string.format("%s", f) -- convert to string, it may be a empty ts node
end

-- Bootstrap lazy.nvim
-- see: https://lazy.folke.io/installation
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

-- Packages
require("lazy").setup({

    -- Show help messages for key bindings,
    {
        'folke/which-key.nvim',
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {
        },
    },

    -- Git integration.
    {
        'tpope/vim-fugitive',
        dependencies = {
            -- Allow bitbucket integration for GBrowse
            'tommcdo/vim-fubitive'
        }
    },

    -- Smart Case substitute, :S, like :s but smarter
    {
        'tpope/vim-abolish'
    },

    -- Toggle between implementation and header files.
    {
        'derekwyatt/vim-fswitch'
    },

    -- Preview markdown files in browser.
    {
        'iamcco/markdown-preview.nvim',
        config = function()
            vim.fn["mkdp#util#install"]()
        end
    },

    -- Zenburn Colors.
    -- {
    --     'jnurmine/zenburn',
    --     config = function()
    --         vim.cmd([[let g:zenburn_disable_Label_underline=1]])
    --         vim.cmd([[colorscheme zenburn]])
    --     end,
    -- },

    -- A more modern (lua) Zenburn implementation
    -- {
    --     'phha/zenburn.nvim',
    --     config = function()
    --         vim.cmd.colorscheme('zenburn')
    --         -- Override some ugly colors
    --         vim.api.nvim_set_hl(0, "StatusLine", { fg="#ccdc90",  bg="#353535" })
    --         vim.api.nvim_set_hl(0, "StatusLineNC", { fg="#88b090",  bg="#353535" })
    --         vim.api.nvim_set_hl(0, "Visual", { bg="#232323" })
    --         vim.api.nvim_set_hl(0, "VisualNOS", { bg="#232323" })
    --     end,
    -- },

    -- Let's try something new.
    {
        'rebelot/kanagawa.nvim',
        lazy = false,
        priority = 1000,
        config = function()
            vim.opt.termguicolors = true
            vim.cmd.colorscheme('kanagawa-dragon')
        end,
    },

    -- Easily toggle comments on/off for selected text.
    {
        'numToStr/Comment.nvim',
        opts = {}
    },

    -- Easy file navigation.
    {
        'tpope/vim-vinegar'
    },

    -- Fuzzy search of almost everything.
    {
        'nvim-telescope/telescope.nvim',
        branch = '0.1.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-tree/nvim-web-devicons'
        },
        opts = {
            defaults = {
                layout_strategy = "vertical",
                mappings = {
                    i = {
                        ["<esc>"] = "close",    -- ESC will close telescope, rather than go into normal mode
                    },
                }
            }
        }
    },

    -- Native FZF for telescope - makes searching faster.
    {
        'nvim-telescope/telescope-fzf-native.nvim',
        build = 'make'
    },

    -- Remove trailing white space on modified lines.
    {
        'axelf4/vim-strip-trailing-whitespace'
    },

    -- Syntax aware stuff like highlighting, current function name, etc.
    {
        'nvim-treesitter/nvim-treesitter',
        config = function()
            vim.cmd([[TSUpdate]])

            local configs = require 'nvim-treesitter.configs'
            configs.setup {
                ensure_installed = {"c", "cpp", "cmake", "rust", "python", "toml", "yaml", "bash", "css", "html", "javascript", "json", "lua"},
                --  enabling highlight module makes prettier code
                highlight = {
                    enable = true,
                    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
                    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
                    -- Using this option may slow down your editor, and you may see some duplicate highlights.
                    -- Instead of true it can also be a list of languages
                    additional_vim_regex_highlighting = false,
                },
            }
        end
    },

    {"nvim-treesitter/nvim-treesitter-context"},
    {"nvim-treesitter/playground"},

    -- Easy LSP configuration, removes a lot of boiler plate.
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        dependencies = {
            -- LSP Support
            {'neovim/nvim-lspconfig'},             -- Required
            {'williamboman/mason.nvim'},           -- Optional
            {'williamboman/mason-lspconfig.nvim'}, -- Optional

            -- Autocompletion
            {'hrsh7th/nvim-cmp'},     -- Required
            {'hrsh7th/cmp-nvim-lsp'}, -- Required
            {'L3MON4D3/LuaSnip'},     -- Required
        }
    },

    -- Dim inactive pane to make it more clear where keyboard focus is.
    -- {
    --     'blueyed/vim-diminactive'
    -- },

    -- Note: requires vim.opt.termguicolors to be set to true
    -- Note: this doesn't seem to work with gruvbox-material, tokyonight, kanagawa
    -- {
    --     'levouh/tint.nvim',
    --     opts = {
    --         tint = -50,
    --         --tint = -10,
    --         --tint_background_colors = true,
    --         saturation = 0.2
    --     },
    --     config = function()
    --         --vim.opt.termguicolors = true
    --     end,
    -- },

    {
        "miversen33/sunglasses.nvim",
        opts = {
            filter_percent = 0.4,
        },
        config = true
    },

    -- Remember which files were open in easy directory vim is started from.
    {
        'rmagatti/auto-session',
        init = function()
            require("auto-session").setup {
                log_level = "error",
                auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/"},
            }
        end
    },

    -- Add indentation guides.
    {
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        opts = {
            indent = {
                char = "┊"
            },
            scope = {
                enabled = false
            }
        },
    },

    -- Let's see what this AI revolution is all about.
    {
        "github/copilot.vim"
    },

    -- It's nice to have confidence when undoing.
    {
        "mbbill/undotree"
    },

    -- Add LSP progress status.
    {
        "j-hui/fidget.nvim",
        tag = "legacy",
        event = "LspAttach",
        opts = {
            text = {
                spinner = "dots",
            }
        },
    },

    -- Translations
    {
        "uga-rosa/translate.nvim",
    },

    -- Satusline
    {
        'nvim-lualine/lualine.nvim',
        dependencies = {
            'nvim-tree/nvim-web-devicons',
        },
        opts = {
            sections = {
                -- left
                lualine_a = { 'mode' },
                lualine_b = { 'branch', 'diff', 'diagnostics' },
                lualine_c = { 'filename', current_treesitter_context },

                -- right
                lualine_x = { 'filetype' },
                lualine_y = { 'progress' },
                lualine_z = { 'location' }
            },
        },
        config = true
    },
})

-- LSP Setup

local lsp = require('lsp-zero')
lsp.preset({
    name = "recommended",
})

lsp.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings to learn the available actions
  vim.keymap.set("n", "gR", function() vim.lsp.buf.rename() end, { buffer = bufnr, desc = "Rename"} )
  vim.keymap.set("n", "ga", function() vim.lsp.buf.code_action() end, { buffer = bufnr, desc = "Code Action"} )
  vim.keymap.set("n", "gr", function() vim.lsp.buf.references() end, { buffer = bufnr, desc = "Show Reference"} )
  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, { buffer = bufnr, desc = "Goto Definition"} )
  vim.keymap.set("n", "gD", function() vim.lsp.buf.declaration() end, { buffer = bufnr, desc = "Goto Declaration"} )
  vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, { buffer = bufnr, desc = "Show Definition"} )
  vim.keymap.set("n", "]d", function() vim.diagnostic.goto_next() end, { buffer = bufnr, desc = "Next Diagnostic"} )
  vim.keymap.set("n", "[d", function() vim.diagnostic.goto_prev() end, { buffer = bufnr, desc = "Previous Diagnostic"} )
end)

require('mason').setup({})
require('mason-lspconfig').setup({
  ensure_installed = {
      -- 'clangd',
      'rust_analyzer',
  },
  handlers = {
    lsp.default_setup,
    lua_ls = function()
      local lua_opts = lsp.nvim_lua_ls()
      require('lspconfig').lua_ls.setup(lua_opts)
    end,
  }
})

local cmp = require('cmp')
local cmp_insert = {behavior = cmp.SelectBehavior.Insert}

cmp.setup({
  mapping = cmp.mapping.preset.insert({
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_insert),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_insert),
    ['<CR>'] = cmp.mapping.confirm({ select = false }),
  })
})

lsp.setup()

vim.g.lsp_zero_ui_float_border = "none" -- don't show the border around floating widows - ugly.

-- local cmp = require('cmp')
-- cmp.setup({
--     mapping = {
--         ['<C-n>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
--         ['<C-p>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
--         ['<CR>'] = cmp.mapping.confirm({select = false}),
--         ['<ESC>'] = cmp.mapping.abort(),
--     }
-- })

-- Telescope Setup
require("telescope").load_extension("fzf")

require("translate").setup({
    default = {
        command = "translate_shell",
    },
})

-- use local clangd instead of the one provided by mason - it gives too many false positives
require('lspconfig')['clangd'].setup {
    cmd = {
        'clangd',
    },
}
