-- Telescope key mappings
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>f", builtin.find_files, { desc = "Find Files" })
vim.keymap.set("n", "<leader>b", function() builtin.buffers({ ignore_current_buffer = true, sort_mru = true }) end, { desc = "Find Buffers" })
vim.keymap.set("n", "<leader>G", function() builtin.grep_string({ word_match = "-w" }) end, { desc = "Grep current word" })
vim.keymap.set("n", "<leader>g", function() builtin.grep_string({ search = vim.fn.input("Grep> ") }) end, { desc = "Grep" } )
vim.keymap.set("n", "<leader>s", builtin.spell_suggest, { desc = "Find Spelling Suggestions" })
vim.keymap.set("n", "<leader>d", function() builtin.lsp_document_symbols({ symbol_width = 100 }) end, { desc = "Find Definitions in File" } )

-- Translations
vim.keymap.set("n", "<leader>t", "<cmd>Translate en -comment<CR>", { desc = "Translate" })

-- Toggle between header and implementation files.
vim.keymap.set("n", "<leader>o", "<cmd>FSHere<CR>")

-- Move blocks of code.
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Line concatenation. Keep cursor where it is.
vim.keymap.set("n", "J", "mzJ`z")

-- Re-center line on page up/down.
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "<C-d>", "<C-d>zz")

-- Search results navigation. Re-center line and open any folds.
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Open Neovim config file
vim.keymap.set("n", "<leader>.", "<cmd>e $MYVIMRC<CR>", { desc = "Edit vim config file" })

-- Clear search highlight with ESC
vim.keymap.set("n", "<esc>", "<esc><cmd>nohlsearch<CR><esc>")

-- I never go into Ex mode on purpose
vim.keymap.set("n", "Q", "<nop>")

-- Quickfix navigation.  Re-center line and open any folds.
vim.keymap.set("n", "<C-j>", "<cmd>cnext<CR>zzzv")
vim.keymap.set("n", "<C-k>", "<cmd>cprev<CR>zzzv")

-- Allow re-syncing of compile_commands.json for a user provided project name
local function sync_comp_db(db)
    local src_base = vim.fn.getcwd() .. '/.build/'
    local proj
    if string.match(src_base, 'corona') then
        proj = 'corona'
    elseif string.match(src_base, 'k2') then
        proj = 'k2'
    elseif string.match(src_base, 'komodo') then
        proj = 'komodo'
    elseif string.match(src_base, 'sake') then
        proj = 'corona'
    end
    local src = src_base .. proj .. '-' .. db .. '/compile_commands.json'
    local err = vim.uv.fs_stat(src)
    if err == nil then
        src = src_base .. db .. '/compile_commands.json'
    end

    local dest = vim.fn.getcwd() .. '/compile_commands.json'

    vim.print(src, dest)
    vim.uv.fs_copyfile(src, dest)

    local escaped_cwd = string.gsub(vim.fn.getcwd(), '/', '\\/')
    os.execute(
    string.format(
    [[sed -i -e's/\/home\/%s\/src\//%s\//g;s/\/home\/%s\/build\//%s\/.build\//g;s/c++ /c++ -ferror-limit=0 /g' %s]],
    proj,
    escaped_cwd,
    proj,
    escaped_cwd,
    dest
    )
    )
end

vim.keymap.set("n", "<leader>p", function()
    local project = vim.fn.input("Project> ")
    vim.cmd.LspStop()
    -- vim.cmd("silent !sync-clangd " .. project)
    sync_comp_db(project)
    vim.cmd.LspStart()
    print("synced compile_commands.json for: " .. project)
end, { desc = "Sync compile_commands.json" })


-- vim.keymap.set('n', '<leader>xc', function()
--   sync_comp_db 'controller'
-- end, { desc = 'Sync [C]ontroller' })
-- vim.keymap.set('n', '<leader>xp', function()
--   sync_comp_db 'red-yaml-parser'
-- end, { desc = 'Sync red-yaml-[P]arser' })
--

vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
