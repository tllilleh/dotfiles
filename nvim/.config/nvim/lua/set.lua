-- Leader Key
vim.g.mapleader = " "

-- Don't wrap lines
vim.opt.wrap = false

-- Use system clipboard by default
vim.opt.clipboard = "unnamedplus"

-- Line Numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- Line Indentation
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true

-- Turn spell-checking on
vim.opt.spell = true
vim.opt.spelllang = "en_us"

-- Start scrolling before we reach end of window
vim.opt.scrolloff = 10
vim.opt.sidescrolloff = 10

-- Enable lazy redraw, this will make things like executing macros faster
vim.opt.lazyredraw = true

-- Use undo files, not swap or backup files
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- Allow 24 bit colors
-- Note: This is required for the tint plugin, but causes color differences in tmux vs terminal
--       The status line in terminal mode has a strange greening tint.
--vim.opt.termguicolors = true


-- Session
vim.opt.sessionoptions="blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions"

