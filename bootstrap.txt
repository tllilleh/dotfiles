Starting with ubuntu 18.04 min install:
- $ sudo apt install git
- $ sudo apt install make
- $ cd ~
- $ git clone https://git@bitbucket.org/tllilleh/dotfiles.git
- $ cd dotfiles
- $ make
- $ checkout/build/install dwm
- $ checkout/build/install slstatus
- $ startx

If using virtual box - install guest additions:
- insert disk into drive
- udiskie mounts into /media/$USER/
- cd /media/$USER/VBox*
- $ sudo ./VBoxLinuxAdditions.run

sudo:
- modify sudoers file to not require password
- $sudo visudo
- add the line:
trent ALL=(ALL) NOPASSWD:ALL

Other applications
- $ sudo apt install firefox
- $ sudo apt install neofetch
- $ sudo apt install docker.io && sudo usermod -a -G docker $USER
- $ sudo apt install repo
- $ sudo apt install subversion
- $ sudo pip install td-watson
- dropbox:
    - download: https://www.dropbox.com/install-linux
    - install:
        - $ sudo apt install PATH_TO_DEB_FILE
        - $ dropbox start -i
- Nerd Font (for Hack): https://github.com/ryanoasis/nerd-fonts
