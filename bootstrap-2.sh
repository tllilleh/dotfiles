#!/bin/bash

if [ -d "$HOME/dotfiles" ]
then
    cd $HOME/dotfiles
    git remote set-url origin bb:tllilleh/dotfiles.git
fi;

# checkout dwm if we haven't already
if [ ! -d "$HOME/proj/dwm" ]
then
    mkdir -p $HOME/proj
    cd $HOME/proj
    git clone bb:tllilleh/dwm.git
    cd dwm
    git config user.name "Trent Lillehaugen"
    git config user.email "tllilleh@gmail.com"
fi;

# checkout slstatus if we haven't already
if [ ! -d "$HOME/proj/slstatus" ]
then
    mkdir -p $HOME/proj
    cd $HOME/proj
    git clone bb:tllilleh/slstatus.git
    cd slstatus
    git config user.name "Trent Lillehaugen"
    git config user.email "tllilleh@gmail.com"
fi;


