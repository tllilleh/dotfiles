#!/bin/bash
maim > /tmp/screen_locked.png
mogrify -brightness-contrast -20x-20 -scale 5% -scale 2000% /tmp/screen_locked.png
i3lock -i /tmp/screen_locked.png
