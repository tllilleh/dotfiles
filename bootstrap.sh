#!/bin/bash

# Create SSH Key
ssh-keygen -f ~/.ssh/id_rsa_bb

# Setup Bitbucket ssh entry
echo "Host bb" >> ~/.ssh/config
echo "  HostName bitbucket.org" >> ~/.ssh/config
echo "  User git" >> ~/.ssh/config
echo "  IdentityFile ~/.ssh/id_rsa_bb" >> ~/.ssh/config
ssh-keygen -R bitbucket.org
ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts

# Install tools required to get/build dotfiles
sudo apt install -y git make

if [ ! -d "$HOME/dotfiles" ]
then
    cd $HOME
    git clone https://bitbucket.org/tllilleh/dotfiles.git
    cd dotfiles
    git config user.name "Trent Lillehaugen"
    git config user.email "tllilleh@gmail.com"
    make
fi;

# Give instructions
echo
echo
echo Bootstrap complete!
echo
echo Next steps:
echo 1\) reboot, or atleast re-login, so all required environment variables are set.
echo 2\) log into bitbucket and associate the following key with account \(~/.ssh/id_rsa_bb.pub\):
cat ~/.ssh/id_rsa_bb.pub
echo 3\) run ~/dotfiles/bootstrap-2.sh

