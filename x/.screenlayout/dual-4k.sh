#!/bin/sh
xrandr --output VIRTUAL1 --off
xrandr --output DP2 --mode 3840x2160 --pos 1920x0 --rotate normal --primary
xrandr --output DP1 --off
xrandr --output HDMI3 --off
xrandr --output HDMI2 --mode 1920x1200 --pos 0x0 --rotate normal
xrandr --output HDMI1 --off
