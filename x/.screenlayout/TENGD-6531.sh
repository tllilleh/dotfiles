#!/bin/sh
xrandr --output VIRTUAL1 --off
xrandr --output DP2 --off
xrandr --output DP1 --off
xrandr --output HDMI3 --off
xrandr --output HDMI2 --mode 2560x1440 --pos 0x0 --rotate normal
xrandr --output HDMI1 --off
