#!/bin/sh
xrandr --output VIRTUAL1 --off
xrandr --output DP2 --off
xrandr --output DP1 --off
#xrandr --output HDMI3 --primary --mode 1920x1200 --pos 3840x0 --rotate normal
#xrandr --output HDMI2 --mode 1920x1200 --pos 0x0 --rotate normal --scale 2x2
xrandr --output HDMI3 --primary --mode 1920x1200 --pos 1920x0 --rotate normal
xrandr --output HDMI2 --mode 1920x1200 --pos 0x0 --rotate normal
xrandr --output HDMI1 --off
