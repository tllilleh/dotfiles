# Setting Up A New Machine

Now based on this template:
https://github.com/rootbeersoup/dotfiles/blob/master/Makefile

## Create an SSH Key:

```bash
ssh-keygen -f ~/.ssh/id_rsa_bb
```

## Add SSH Key to Bit Bucket Profile

Show key:

```bash
cat ~/.ssh/id_rsa_bb.pub
```
Then, copy and paste into profile/ssh keys page: https://bitbucket.org/account/user/tllilleh/ssh-keys/

## Add entry to SSH configuration:

```bash
echo "Host bb" >> ~/.ssh/config
echo "  HostName bitbucket.org" >> ~/.ssh/config
echo "  User git" >> ~/.ssh/config
echo "  IdentityFile ~/.ssh/id_rsa_bb" >> ~/.ssh/config

ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
```

Note: on Windows 10 (with Ubuntu), the default permissions will be wrong:
```bash
chmod 600 ~/.ssh/config
```

Note: on Mac:
```bash
ssh-add -K ~/.ssh/id_rsa_bb
```

## Clone Repo / Git Configuration

```bash
cd ~
git clone bb:tllilleh/dotfiles.git
cd dotfiles
git config user.name "Trent Lillehaugen"
git config user.email "tllilleh@gmail.com"
```

## Install

```bash
make install
```

## Uninstall

```bash
make uninstall
```

## Mac

### Setup Terminal Theme

Themes are found in ~/terminal

